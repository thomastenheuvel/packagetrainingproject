def multiply(a: int, b: int) -> float:
    """
    Function to multiply two numbers.

    Args:
        a (int): First number to multiply.
        b (int): Second number to multiply.

    Returns:
        float: a multiplied by b.
    """
    return a*b

def substract(a: int, b: int) -> float:
    """
    Function to substract two numbers.

    Args:
        a (int): First number.
        b (int): Second number.

    Returns:
        float: a substracted by b.
    """
    return a-b

def add(a: int, b: int) -> int:
    return a+b
