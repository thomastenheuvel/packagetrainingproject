from setuptools import setup, find_packages

setup(
    name='MyPackage',
    version='0.1.0',
    python_requires='>=3.9',
    description='This is my first package',
    author='Thomas ten Heuvel',
    packages=find_packages(),
    install_requires=[
        "numpy",
        "pandas"
    ]
)